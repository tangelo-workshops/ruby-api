# frozen_string_literal: true

require_relative '../config/db'

# Handles the user call, no matter the source
class BaseHandler

    def db_client
        Mysql.client
    end
end
