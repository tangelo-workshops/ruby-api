# frozen_string_literal: true

require_relative 'base'
require_relative '../config/db'

# Handles the user call, no matter the source
class UserHandler < BaseHandler

  def initialize(hash)
    @hash = hash
  end

  def run
    update_hash
    #call_hash_api
    {'message': 'inserted correctly'}
  end
  
  def insert_user
    db_client[:users].insert(
      username: @username,
      email: @email,
      text: @text
  )
  end

  def call_hash_api
    Net::HTTP.post URI('http://localhost/api/v1/hash'),
                  { "text" => @text }.to_json,
                  "Content-Type" => "application/json"
  end
end
