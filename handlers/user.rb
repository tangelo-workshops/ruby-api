# frozen_string_literal: true

require_relative 'base'
#require_relative '../utils/helpers'
require_relative '../config/db'
require 'net/http'
require 'uri'
require_relative '../config/constants'

# Handles the user call, no matter the source
class UserHandler < BaseHandler
  #include Utils

  def initialize(username, email, text)
    @username = username
    @email = email
    @text = text
  end

  def run
    insert_user
    #call_hash_api
    {'message': 'inserted correctly'}
  end
  
  def insert_user
    db_client[:users].insert(
      username: @username,
      email: @email,
      text: @text
  )
  end

  def call_hash_api
    Net::HTTP.post URI(System::HASH_URI),
                  { "text" => @text }.to_json,
                  "Content-Type" => "application/json"
  end
end
