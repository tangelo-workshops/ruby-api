# frozen_string_literal: true

require_relative '../config/db'
require 'sequel'


DB = Sequel.connect(
    adapter: 'mysql2',
    host: System::MYSQL_HOST, 
    password: System::MYSQL_PASSWORD, 
    user: System::MYSQL_USER,
    database: System::MYSQL_DB
)

DB.create_table :users do
    primary_key :id
    column :username, String
    column :email, String
    column :text, String
    column :hash, String
end
