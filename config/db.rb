# frozen_string_literal: true

require 'sequel'
require_relative 'constants'

#Mysql module
module Mysql
    def Mysql.client
        Sequel.connect(
            adapter: 'mysql2',
            host: System::MYSQL_HOST, 
            password: System::MYSQL_PASSWORD, 
            user: System::MYSQL_USER,
            database: System::MYSQL_DB
        )
    end
end