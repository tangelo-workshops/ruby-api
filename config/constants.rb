# frozen_string_literal: true

# System constants
module System
    MYSQL_HOST = ENV['MYSQL_HOST']
    MYSQL_USER = ENV['MYSQL_USER']
    MYSQL_PORT = ENV['MYSQL_PORT']
    MYSQL_PASSWORD = ENV['MYSQL_PASSWORD']
    MYSQL_DB = ENV['MYSQL_DB']
    HASH_URI = ENV['HASH_URI']
end