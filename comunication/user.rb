# frozen_string_literal: true

require_relative '../handlers/user'

# Cumunication api module that receive user data
module UserAPI
  # Api grape default class
  class APIv1 < Grape::API
    version 'v1', using: :path
    format :json

    namespace 'user' do
      params do
        requires :username, type: String, desc: 'Username'
        requires :email, type: String, desc: 'User Email'
        requires :text, type: String, desc: 'Text'
      end
      post do
        handler = UserHandler.new(
          params[:username],
          params[:email],
          params[:text]
        )
        handler.run
      end
    end
  end
end
