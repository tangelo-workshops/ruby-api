# frozen_string_literal: true

require_relative '../handlers/user'

# Cumunication api module that receive user data
module HashAPI
  # Api grape default class
  class APIv1 < Grape::API
    version 'v1', using: :path
    format :json

    namespace 'hash' do
      params do
        requires :response, type: Hash, desc: 'Response from third party'
      end
      post do
        handler = HashHandler.new(
          params[:response][:hash]
        )
        handler.run
      end
    end
  end
end
