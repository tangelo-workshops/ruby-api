# frozen_string_literal: true

require 'grape'
require_relative 'comunication/hash'
require_relative 'comunication/user'

module ConsumerAPI
  class API < Grape::API
    prefix :api
    mount UserAPI::APIv1
  end
end
